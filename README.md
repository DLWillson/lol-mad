# LOL MAD

Learn MAD over lunch, featuring Prometheus, Grafana, and friends.

![MAD with Prometheus, Grafana, and Friends](MAD.png)

## Talk

[Slides](Mad-Talk.fodp)

## A Few Simple Exercises - See the Demo, Do the Pair

```
shajimuowei
```

### Use [Blackbox Exporter](http://blackbox_exporter.petersons.com)

a fast site:
- site: http://www.petersons.com/blog/
- probe-and-export: http://blackbox_exporter.petersons.com/probe?target=http%3A%2F%2Fwww.petersons.com%2Fblog%2F&module=http_2xx

a slow site:
- site: http://superdupergames.org/main.html?page=play_homeworlds&num=36261
- probe-and-export: http://blackbox_exporter.petersons.com/probe?target=http%3A%2F%2Fsuperdupergames.org%2Fmain.html%3Fpage%3Dplay_homeworlds%26num%3D36261&module=http_2xx

Compare `probe_duration_seconds` of both sites.


### Use [Prometheus](http://prometheus.petersons.com):

Graph this query:

```PromQL
probe_duration_seconds{instance="http://materdei.petersons.com/Account/Login"}
```


### Use [Grafana](http://grafana.petersons.com):

Make a personal folder for your Dashboards

Import Dashboard ID '11543' or another of your choice from https://grafana.com/grafana/dashboards

Create a new Dashboard and chart the same query from the Prometheus section


## Share

Revisit the Q&A from the slides, then Q&A

---

Do you want to know <blink>MORE</blink>?
- [Grafana Fundamentals](https://grafana.com/tutorials/grafana-fundamentals/)
- [Prometheus Getting Started](https://prometheus.io/docs/prometheus/latest/getting_started/)
